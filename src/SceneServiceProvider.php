<?php

namespace DecideNow\Scene;

use Illuminate\Support\ServiceProvider;
use DecideNow\Scene\Commands\SceneCreate;

class SceneServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
		//
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    	$this->loadViewsFrom(__DIR__.'/Commands/templates', 'command_templates');
		$this->loadViewsFrom(__DIR__.'/Views/scene', 'scene');
		$this->loadViewsFrom(__DIR__.'/Views/stage', 'stage');
		$this->loadViewsFrom(__DIR__.'/Views/vendor', 'scene-views');
		$this->publishes([
			__DIR__.'/Assets' => public_path(),
		], 'scene-assets');
		$this->publishes([
			__DIR__.'/Config/scene.php' => config_path('scene.php'),
		], 'scene-config');
		$this->publishes([
			__DIR__.'/Views/vendor' => resource_path('views/vendor/scene'),
		], 'scene-views');
		$this->commands([
			SceneCreate::class,
		]);
    }
}
