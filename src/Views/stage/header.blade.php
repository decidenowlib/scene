@php
	$scene_extensions = config('scene.extensions', []);
@endphp

<meta name="csrf-token" content="{{ csrf_token() }}">

@foreach ($scene_extensions as $extension)
	@if(View::exists($extension.'::header'))
		@include($extension.'::header')
	@endif
@endforeach

@include('scene::style')

@foreach ($scene_extensions as $extension)
	@if(View::exists($extension.'::style'))
		@include($extension.'::style')
	@endif
@endforeach