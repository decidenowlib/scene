@php
	$scene_extensions = config('scene.extensions', []);
@endphp

@include('scene::content', $scene->sceneVariables())

@include('stage::group_scripts')
@include('stage::group_objects')
@include('scene::code')
@include('stage::group_code')
