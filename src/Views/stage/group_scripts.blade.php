@push('stage::code')

	@include('stage::scripts')
	
	@foreach ($scene_extensions as $extension)
		@if(View::exists($extension.'::scripts'))
			@include($extension.'::scripts')
		@endif
	@endforeach

@endpush