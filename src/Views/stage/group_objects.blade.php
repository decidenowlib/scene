@push('stage::code')
	@include('stage::object')
	
	@if (count($scene_extensions))
		@foreach ($scene_extensions as $extension)
			@if(View::exists($extension.'::object'))
				@include($extension.'::object')
			@endif
		@endforeach
	@endif
	
	<script>
		DecideNowObjects = { StageClass: StageClass, SceneClass: SceneClass, stage: new StageClass() };
	</script>

@endpush