@push('stage::code')

	@if (count($scene_extensions))
		@foreach ($scene_extensions as $extension)
			@if(View::exists($extension.'::code'))
				@include($extension.'::code')
			@endif
		@endforeach
	@endif

@endpush