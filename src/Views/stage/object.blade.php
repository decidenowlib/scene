<script>
	// Stage object
	
	StageClass = function () {
	
		this.scenes = {};
		
		this.addScene = function (scene) {
			this.scenes[scene.id] = scene;
		}
		
		this.getScene = function (scene_id) {
			return this.scenes[scene_id];
		}
	
		this.isBusy = function () {
			for (key in this.scenes) {
				if (this.scenes[key].is_busy) {
					return key;
				}
			}
			return false;
		}
		
		this.stub = function (param) {
			console.log(param + ', base');
		}
	}
	
	
	// Scene object
	
	SceneClass = function (scene_id, extension, options) {
		
		this.default_options = {
			method : 'POST',
			task : '',
			secretly : false,
			technology : 'standard',
			busy_icon: 'scene-busy-icon',
			form: {},
			target: '',
			form_selector: '[data-role="data_form"]',
			request_context: {},
		};
		this.options = options || {};
		for (default_option in this.default_options) {
			if (!(default_option in this.options)) {
				this.options[default_option] = this.default_options[default_option];
			}
		}
		
		this.getRoot = function () {
			return $('[data-scene-id="'+this.id+'"]');
		}
		
		this.getElement = function (selector) {
			var scene_el = this.getRoot();
			var children = scene_el.find('[data-scene-id]');
			var in_children = children.find(selector, children);
			var ret = scene_el.find(selector).not(in_children);
			return ret;
		}
		
		this.isBusySet = function (secretly, icon) {
			this.is_busy = true;
			if (!secretly) {
				if (!icon) {
					icon = this.option.busy_icon;
				}
				this.getRoot().append('<div class="scene-loading"><i class="'+icon+'"></i></div>');
			}
		}
		
		this.isBusyUnset = function () {
			this.is_busy = false;
			this.getElement('.scene-loading').remove();
		}
		
		this.serializeFormData = function(data, empty) {
			empty = empty || false;
			data = data || {};
			var form_selector = data.form_selector || this.options.form_selector;
			var form_el = this.getElement(form_selector);
			
			var form_data = form_el.find('input:not([name="_token"]), select, textarea').serializeArray().concat(
				form_el.find('input[type="checkbox"]:not(:checked)').map(function() {
					// add unchecked checkboxes names
					return { name: this.name, value: '' };
				}).get()
			);
		    
		    // empty all fields
			if (empty) {
				form_data.map(function (el) {
					el.value = '';
					return el;
				});
			}
			
			return form_data;
		}
		
		this.getFormData = function(data, empty) {
			
			var ret = {};
			var tmp_data = this.serializeFormData(data, empty);
			tmp_data.push({ name: 'scene_id', value: (tmp_data.scene_id || this.id) });
			tmp_data.push({ name: '_token', value: $('meta[name=csrf-token]').attr('content') });
			for (key in tmp_data) {
				var field = tmp_data[key];
				ret[field.name] = field.value;
			}
			return ret;
		},
		
		this.defaultSceneRefresh = function () {
			
			var data = {};
			var request_context = {};
			var default_url = null;
			var filesData = null;
			var onSuccess = null;
			var onError = null;
			
			if (arguments.length === 0) {
				console.error('First parameter should contain Url!');
				return;
			}
			default_url = arguments[0];
			
			if (arguments.length > 1) {
				data = arguments[1];
			}
			if (arguments.length > 2) {
				if (typeof arguments[2] === 'function') {
					if (arguments.length === 3) {
						onSuccess = arguments[2];
					} else if (arguments.length === 4) {
						onSuccess = arguments[2];
						onError = arguments[3];
					} else if (arguments.length === 5) {
						filesData = arguments[2];
						onSuccess = arguments[3];
						onError = arguments[4];
					}
				} else {
					request_context = arguments[2];
					if (arguments.length === 4) {
						onSuccess = arguments[3];
					} else if (arguments.length === 5) {
						onSuccess = arguments[3];
						onError = arguments[4];
					} else if (arguments.length === 6) {
						filesData = arguments[3];
						onSuccess = arguments[4];
						onError = arguments[5];
					}
				}
			}
			
			post_data = {};
			post_data.scene_id = data.scene_id || this.id;
			post_data.url = data.url || default_url;
			post_data.method = data.method || this.options.method;
			post_data.task = data.task || this.options.task;
			post_data.secretly = data.secretly || this.options.secretly;
			post_data.target = data.target || this.options.target;
			post_data.technology = data.technology || this.options.technology;
			post_data.form = data.form || this.options.form;
			
			if (post_data.technology == 'ajax') {
				if (filesData) {
					this.sendRequestAJAX(post_data, request_context, filesData, onSuccess, onError);		
				} else {
					this.sendRequestAJAX(post_data, request_context,  onSuccess, onError);
				}
			} else {
				this.sendRequest(post_data);			
			}
		}
		
		this.sendRequest = function (data) {
			if (arguments.length === 0) {
				console.error('First parameter should contain Data!');
				return;
			}
			data = arguments[0];
			if (!data.url) {
				console.error('"url" value should be present!');
				return;
			}
			
			post_data = {};
			post_data.url = data.url;
			post_data.method = data.method || this.options.method;
			post_data.target = data.target || this.options.target;
			post_data.secretly = data.secretly || this.options.secretly;
			post_data.busy_icon = data.busy_icon || this.options.busy_icon;
			post_data.form = data.form || this.options.form;
			
			var form_selector = data.form_selector || this.options.form_selector;
			var form_el = this.getElement(form_selector);
			if (!form_el.length) {
				console.err('Form element not found (' + form_selector + ')');
			}
			
			form_el.attr('action', post_data.url)
			form_el.attr('target', post_data.target)
			form_el.attr('method', post_data.method)
			
			var appended = [];
			
			// add hidden input for every unchecked checkbox
			form_el.find('input[type="checkbox"]:not(:checked)').map(function() {
				var field = $('<input>', {
					type: 'hidden',
					name: this.name,
					value: ''
				});
				field.appendTo(form_el);
				appended.push(field);
			});
			
			for (key in post_data.form) {
				var value = post_data.form[key];
				if (Array.isArray(value)) {
					for (val_key in value) {
						var field = $('<input>', {
							type: 'hidden',
							name: key + '[]',
							value: this.escapeFormValue(value[val_key])
						});
						field.appendTo(form_el);
						appended.push(field);
					}
				} else {
					var field = $('<input>', {
						type: 'hidden',
						name: key,
						value: this.escapeFormValue(value)
					});
					field.appendTo(form_el);
					appended.push(field);
				}
			}

			this.isBusySet(post_data.secretly, post_data.busy_icon);
			form_el.submit();
		}
		
		this.sendRequestAJAX = function () {
			
			if (busy_scene = DecideNowObjects.stage.isBusy()) {
				console.error('Stage is busy! (' + busy_scene + ')');
				return;
			}
			var data = {};
			var request_context = {};
			var filesData = null;
			var onSuccess = null;
			var onError = null;
			
			if (arguments.length === 0) {
				console.error('First parameter should contain Data!');
				return;
			}
			data = arguments[0];
			if (!data.url) {
				console.error('"url" value should be present!');
				return;
			}
			
			if (arguments.length > 1) {
				if (typeof arguments[1] === 'function') {
					if (arguments.length === 2) {
						onSuccess = arguments[1];
					} else if (arguments.length === 3) {
						onSuccess = arguments[1];
						onError = arguments[2];
					} else if (arguments.length === 4) {
						filesData = arguments[1];
						onSuccess = arguments[2];
						onError = arguments[3];
					}
				} else {
					request_context = arguments[1];
					if (arguments.length === 3) {
						onSuccess = arguments[2];
					} else if (arguments.length === 4) {
						onSuccess = arguments[2];
						onError = arguments[3];
					} else if (arguments.length === 5) {
						filesData = arguments[2];
						onSuccess = arguments[3];
						onError = arguments[4];
					}
				}
			}
			
			post_data = {};
			post_data._token = $('meta[name=csrf-token]').attr('content');
			post_data.url = data.url;
			post_data.method = data.method || this.options.method;
			post_data.secretly = data.secretly || this.options.secretly;
			post_data.busy_icon = data.busy_icon || this.options.busy_icon;
			post_data.form = data.form || this.options.form;
			
			//onSuccess = onSuccess || this.defaultOnSuccessAJAX;
			//onError = onError || this.defaultOnErrorAJAX;
			
			var formData = new FormData();
			for (key in post_data.form) {
				var value = post_data.form[key];
				if (Array.isArray(value)) {
					for (val_key in value) {
						formData.append(key + '[' + val_key + ']', this.escapeFormValue(value[val_key]));
					}
				} else {
					formData.append(key, this.escapeFormValue(value));
				}
			}
			var form_data = this.serializeFormData(data);
			form_data.push({ name: 'scene_id', value: (data.scene_id || this.id) });
			form_data.push({ name: '_token', value: post_data._token });
			for (key in form_data) {
				var field = form_data[key];
				if (!formData.has(field.name)) {
					formData.append(field.name, field.value);
				}
			}
			if (filesData) {
				var files = [];
				var lng = filesData.length;
				for (i = 0; i < lng; i++) {
					formData.append('files['+i+']', filesData[i]);
				}
			}
			
			this.isBusySet(post_data.secretly, post_data.busy_icon);
			
			var ajax_data = {
				type: post_data.method,
				url: post_data.url,
				data: formData,
				contentType: false,
				processData: false,
				context: {
					thisScene: this,
					secretly: post_data.secretly,
					request_context,
				},
				success: function(response) {
					this.thisScene.processResponseAJAX(this, response, onSuccess, this.thisScene.defaultOnSuccessAJAX);
				},
				error: function(response) {
					this.thisScene.processResponseAJAX(this, response, onError, this.thisScene.defaultOnErrorAJAX);
				}
			};
			
			$.ajax(ajax_data);
		}
		
		this.showMessages = function (messages, message_type, alert_errors, has_content) {
			if ( messages.success && (typeof message_type === 'undefined' || message_type === 'success') ) {
				for (msg_key in messages.success) {
					var real_msg = messages.success[msg_key];
					var out_msg = real_msg;
					if (messages.tags && messages.tags.success && (msg_key in messages.tags.success)) {
						out_msg = real_msg + ' (' + messages.tags.success[msg_key] + ')';
					}
					console.log(out_msg);
				}
			}
			if ( messages.warning && (typeof message_type === 'undefined' || message_type === 'warning') ) {
				for (msg_key in messages.warning) {
					var real_msg = messages.warning[msg_key];
					var out_msg = real_msg;
					if (messages.tags && messages.tags.warning && (msg_key in messages.tags.warning)) {
						out_msg = real_msg + ' (' + messages.tags.warning[msg_key] + ')';
					}
					console.warn(out_msg);
				}
			}
			if ( messages.error && (typeof message_type === 'undefined' || message_type === 'error') ) {
				for (msg_key in messages.error) {
					var real_msg = messages.error[msg_key];
					var out_msg = real_msg;
					if (messages.tags && messages.tags.error && (msg_key in messages.tags.error)) {
						out_msg = real_msg + ' (' + messages.tags.error[msg_key] + ')';
					}
					console.error(out_msg);
					if (alert_errors) {
						alert(real_msg);
					}
				}
			}
		}
		
		this.defaultOnSuccessAJAX = function (response, context) {
			var thisScene = response.thisScene;
			var scene_root = thisScene.getRoot();
			
			if (response.content) {
				scene_root.html(response.content);
			}
			if (response.messages) {
				thisScene.showMessages(response.messages, undefined, false, ((response.content) ? true : false));
			}
			if (response.content) {
				thisScene.init();
			}
		}
		
		this.defaultOnErrorAJAX = function (response, context) {
			var thisScene = response.thisScene;
			
			var resp = response.responseJSON;
			if (resp.messages) {
				thisScene.showMessages(resp.messages, undefined, true, false);
			}
		}
		
		this.processResponseAJAX = function(context, response, callback, default_callback) {
				
			if (!response) {
				console.warn('Scene response is empty!');
				response = {};
			}
			if (response !== Object(response)) {
				console.warn('Scene response is not an Object!');
				response = {};
			}
			
			context.thisScene.isBusyUnset();
			
			response.thisScene = context.thisScene;
			response.secretly = context.secretly;
			
			if (!context.request_context.permit_default) {
				default_callback(response, context.request_context);
			}
			if (callback) {
				callback(response, context.request_context);
			}
		}
		
		this.escapeFormValue = function(value) {
			if (typeof value === 'boolean') {
				value = this.escapeBoolean(value);
			}
			return value;
		},
		
		this.escapeHtml = function (text) {
			if (typeof(text) !== 'string') {
				return text;
			}
			var map = { '&': '&amp;', '<': '&lt;', '>': '&gt;', '"': '&quot;', "'": '&#039;' };
			return text.replace(/[&<>"']/g, function(m) { return map[m]; });
		}
		
		this.escapeBoolean = function (value) {
			if (value == 'false' || value == 'null' || value == '0' || value == '') {
				return '';
			} else {
				return value;
			}
		}
		
		this.addActionTechnology = function(el, data) {
			data = data || {};
			var tech = $(el).data('action-tech');
			data.technology = (tech) ? tech : 'standard';
			return data;
		},
		
		this.defaultInit = function() {
			//
		}
		
		this.init = function() {
			this.defaultInit();
		}
			
		/* Scene "constructor" */
		
		this.id = scene_id;
		this.is_busy = false;
		this.technology = 'standard';
		if (DecideNowObjects.stage != null && this.id) {
			DecideNowObjects.stage.addScene(this);
		}
		
		for (key in extension) {
			this[key] = extension[key];
		}
	
	}
	
</script>
