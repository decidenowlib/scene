<div data-scene-id="{{ $scene->sceneId() }}">
	@includeWhen($scene->has_content, $scene->getTemplateContent() )
</div>