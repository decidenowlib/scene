<style>
	[data-scene-id] {
		position: relative;
	}
	[data-scene-id] .scene-loading {
		background-color: rgba(255, 255, 255, .8);
		position: absolute;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		z-index: 5;
		text-align: center;
	}
	
	/* https://projects.lukehaas.me/css-loaders/ */
	[data-scene-id] .scene-busy-icon {
		display: block;
		margin: 20px auto;
		font-size: 7px;
		position: relative;
		text-indent: -9999em;
		border-top: .7em solid rgba(70,70,70, 0.2);
		border-right: .7em solid rgba(70,70,70, 0.2);
		border-bottom: .7em solid rgba(70,70,70, 0.2);
		border-left: .7em solid rgba(70,70,70);
		-webkit-transform: translateZ(0);
		-ms-transform: translateZ(0);
		transform: translateZ(0);
		-webkit-animation: load8 1.1s infinite linear;
		animation: load8 1.1s infinite linear;
	}
	[data-scene-id] .scene-busy-icon,
	[data-scene-id] .scene-busy-icon:after {
		border-radius: 50%;
		width: 5em;
		height: 5em;
	}
	@-webkit-keyframes load8 {
		0% {
			-webkit-transform: rotate(0deg);
			transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
	@keyframes load8 {
		0% {
			-webkit-transform: rotate(0deg);
			transform: rotate(0deg);
		}
		100% {
			-webkit-transform: rotate(360deg);
			transform: rotate(360deg);
		}
	}
</style>