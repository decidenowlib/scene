@push('stage::code')

	@foreach($scene->childrenFlatten() as $subscene)
		@if ($subscene->getTemplateCode())
			@include($subscene->getTemplateCode(), $subscene->sceneVariables())
		@endif
	@endforeach

@endpush