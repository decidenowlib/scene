<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>{{ (isset($page_title) && $page_title) ? $page_title : config('app.name') }}</title>
        @if (isset($page_description) && $page_description)
        	 <meta name="description" content="{{ $page_description }}">
        @endif
        @if (isset($page_keywords) && $page_keywords)
        	 <meta name="keywords" content="{{ $page_keywords }}">
        @endif
        @include('stage::header')
    </head>
    <body>
        @include('stage::content')
        @stack('stage::code')
    </body>
</html>
