<?php

return [
    'extensions' => [
    	//
    ],
	'default_layout' => 'scene-views::layouts.scene-default.layout',
	'page_title' => 'Laravel Scene',
	'page_decription' => 'Scene-based architecture library for Laravel',
	'page_keywords' => 'laravel, scene, decidenow',
	'user_bootstrap_3' => '0',
];
