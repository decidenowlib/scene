<script>

new DecideNowObjects.SceneClass('{!! (true) ? '{' : '' !!}{ $scene->sceneId() }}', {

	/* init */
	
	init: function() {
		var thisScene = this;
		thisScene.defaultInit();
		
		thisScene.getElement('[id="btn-refresh"]').click( { thisScene }, thisScene.btnRefresh_Click );
		thisScene.getElement('[id="btn-some-task"]').click( { thisScene }, thisScene.btnSomeTask_Click );
	},


	/* refresh */
	
	sceneRefresh : function(data) {
		var thisScene = this;
		var refresh_url = '{!! (true) ? '{' : '' !!}{ url($scene->sceneURL()) }}';
		thisScene.defaultSceneRefresh(refresh_url, data);
	},
	
	
	//
	
	btnRefresh_Click : function (e) {
		e.preventDefault();
		thisScene = e.data.thisScene;
		thisScene.sceneRefresh(thisScene.addActionTechnology(e.currentTarget));
		return false;
	},
	
	btnSomeTask_Click : function (e) {
		e.preventDefault();
		thisScene = e.data.thisScene;
		thisScene.sceneRefresh(thisScene.addActionTechnology(e.currentTarget, { form: { task: 'some_task' } }));
		return false;
	},
	
}).init();

</script>