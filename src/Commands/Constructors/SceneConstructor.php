<?php

namespace DecideNow\Scene\Commands\Constructors;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class SceneConstructor
{
    public function __construct($scene_key, $force_create = false)
    {
        $this->scene_key = $scene_key;
        $this->force_create = $force_create;

        $this->setSceneParts();
        $this->setConntrollerData();
        $this->setViewData();
        $this->setRouteData();
    }

    public $scene_key;
    public $force_create;

    public $template_controller = 'command_templates::scene_controller';
    public $template_code = 'command_templates::scene_code';
    public $template_content = 'command_templates::scene_content';
	
	
	private $scene_parts = [];
	public function setSceneParts()
	{
		$scene_parts = [];
		$scene_parts_data = explode('-', $this->scene_key);
		foreach ($scene_parts_data as $scene_part) {
			$part_lower = Str::lower($scene_part);
			$part_camel = Str::camel($part_lower);
			$part_pascal = Str::ucfirst($part_camel);
			$part_snake = Str::snake($part_pascal);
			$scene_parts[] = (object) ['lower' => $part_lower, 'camel' => $part_camel, 'pascal' => $part_pascal, 'snake' => $part_snake];
		}
		$this->scene_parts = $scene_parts;
	}
	public function getSceneParts()
	{
		return $this->scene_parts;
    }
    
    private $controller_data = [];
    public function setConntrollerData()
	{
		$scene_parts = $this->getSceneParts();
		$idx = 0;
		$last_id = count($scene_parts) - 1;
		
		$name = '';
		$path = '';
		$namespace = '';

		foreach ($scene_parts as $scene_part) {
			$name .= $scene_part->pascal;
			$path .= ($idx == $last_id) ? '' : ('/' . $scene_part->pascal);
			$namespace .= ($idx == $last_id) ? '' : ('\\' . $scene_part->pascal);
			$idx++;
		}

		$file_path = app_path('Http/Controllers' . $path);
		$file_name = $file_path . '/' . $name.'Controller.php';
		$namespace_full = 'App\\Http\\Controllers' . $namespace;
		$name_relative = substr($namespace, 1) . '\\' . $name . 'Controller';

		$this->controller_data = [
			'file_path' => $file_path,
			'file_name' => $file_name,
			'namespace' => $namespace_full, 
			'name' => $name,
			'name_relative' => $name_relative,
		];
    }
    public function getControllerData()
    {
        return $this->controller_data;
    }

    private $view_data = [];
    public function setViewData()
    {
        $scene_parts = $this->getSceneParts();
        
		$name = '';
		$path = '';

		foreach ($scene_parts as $scene_part) {
            $name .= '-' . $scene_part->lower;
			$path .= '/' . $scene_part->lower;
		}

		$name = substr($name, 1);
		$file_path = resource_path('views' . $path);
		$file_name_content = $file_path . '/' . $name . '_content.blade.php';
		$file_name_code = $file_path . '/' . $name . '_code.blade.php';

		$this->view_data = [
			'file_path' => $file_path,
			'file_name_content' => $file_name_content,
			'file_name_code' => $file_name_code,
		];
    }
    public function getViewData()
    {
        return $this->view_data;
    }

    private $route_data = [];
    public function setRouteData()
    {
        $scene_parts = $this->getSceneParts();
        
		$route = '';

		foreach ($scene_parts as $scene_part) {
            $route .= '/' . str_replace('_', '-', $scene_part->lower);
		}

		$route_get = 'Route::get(\'' . $route . '\', \'' . $this->controller_data['name_relative'] . '@get\');';
		$route_post = 'Route::post(\'' . $route . '\', \'' . $this->controller_data['name_relative'] . '@post\');';

		$this->route_data = [
			'url' => $route,
			'route_get' => $route_get,
			'route_post' => $route_post,
		];
    }
    public function getRouteData()
    {
        return $this->route_data;
    }

    public function createController()
    {
        $data = [
			'controller_namespace' => $this->controller_data['namespace'], 
			'controller_name' => $this->controller_data['name'], 
			'scene_key' => $this->scene_key, 
		];
		if (!File::exists($this->controller_data['file_path'])) {
			File::makeDirectory($this->controller_data['file_path'], 0755, true);
		}
		if (!File::exists($this->controller_data['file_name']) || $this->force_create) {
			File::put($this->controller_data['file_name'], View::make($this->template_controller, $data)->render());
		}
    }

    public function createView()
    {
        $data = [
			'scene_key' => $this->scene_key,
		];
		
		if (!File::exists($this->view_data['file_path'])) {
			File::makeDirectory($this->view_data['file_path'], 0755, true);
		}
		if (!File::exists($this->view_data['file_name_code']) || $this->force_create) {
			File::put($this->view_data['file_name_code'], View::make($this->template_code, $data)->render());
		}
		if (!File::exists($this->view_data['file_name_content']) || $this->force_create) {
			File::put($this->view_data['file_name_content'], View::make($this->template_content, $data)->render());
		}
    }

	public function addRoutes($routes, $prefix_line = '')
	{
		$missing_routes = $routes;
		$routes_path = base_path('routes/web.php');
		foreach (file($routes_path) as $line) {
			$not_missing = [];
			foreach ($routes as $route_key => $route) {
				if (strpos($line, $route) !== false) {
					$not_missing[] = $route_key;
				}
			}
			if (count($not_missing)) {
				foreach ($not_missing as $route_key) {
					unset($missing_routes[$route_key]);
				}
			}
		}

		$new_lines = [];
		if (count($missing_routes) == count($routes) && $prefix_line) {
            $new_lines[] = '';
			$new_lines[] = '// ' . $prefix_line;
			$new_lines[] = '';
		}
		
		foreach ($missing_routes as $missing_route) {
			$new_lines[] = $missing_route;
		}

		if (count($new_lines)) {
			File::append($routes_path, "\r\n");
		}
		foreach ($new_lines as $new_line) {
			File::append($routes_path, $new_line . "\r\n");
		}
	}

    public function createRoute($prefix_line = '')
    {
		$routes = [
			$this->route_data['route_get'],
			$this->route_data['route_post']
		];
		$this->addRoutes($routes, $prefix_line);
    }

    public function beforeCreate()
    {
        //
    }

    public function afterCreate()
    {
        //
    }

    public function create($add_route_prefix = true, $create_controller = true, $create_view = true, $create_route = true)
    {
        $this->beforeCreate();
        if ($create_controller) {
            $this->createController();
        }
        if ($create_view) {
            $this->createView();
        }
        if ($create_route) {
            $this->createRoute($add_route_prefix ? $this->controller_data['name'] : '');
        }
        $this->afterCreate();
    }
}