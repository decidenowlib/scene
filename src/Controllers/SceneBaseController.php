<?php
namespace DecideNow\Scene\Controllers;

use DecideNow\Scene\Messages\SceneMessages;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;

class SceneBaseController extends BaseController {
	
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	
	public $page_title = '';
	public $page_description = '';
	public $page_keywords = '';

	public static $scene_key = '';
	
	public $template_content = '';
	public $template_code = '';
	public $layout = '';
	
	public $scene_url;
	
	public static $default_actions_technology = 'standard';
	public static $actions_technologies = [];
	
	public $scene_id = '';
	public $scene_id_suffix = '';
	
	public $scene_parent = null;
	public $scene_children = null;
	
	public $scene_technology = 'standard';
	
	public $task = '';
	public $is_nested = false;
	public $state_permanent = [['name' => 'task', 'frontend_hidden' => true], 'is_nested'];
	public $state = [];
	
	public $has_content = false;
	public $has_content_error = false;

	public function __get($name) 
    {
        if ($name === 'no_content ') {
            return !$this->has_content;
		}
	}
	
	public $messages;
	
	
	public function __construct($scene_parent = null, $scene_id = '', $scene_id_suffix = '')
	{
		$this->page_title = config('scene.page_title');
		$this->page_description = config('scene.page_decription');
		$this->page_keywords = config('scene.page_keywords');
		
		$this->scene_id = ($scene_id) ?: static::$scene_key;
		$this->scene_id_suffix = $scene_id_suffix;
		$this->scene_parent = $scene_parent;
		$this->is_nested = ($this->scene_parent) ? true : false;
		$this->scene_children = new Collection();
		$this->messages = new SceneMessages();
		$cnt = 0;
		$parent = $this->scene_parent;
		while ($parent) {
			$parent = $parent->scene_parent;
			$cnt++;
		}
		if ($cnt < 10) {
			$this->childrenDefine();
		}
	}
	
	
	public function childrenDefine()
	{
		//
	}
	
	public function childrenFlatten()
	{
		$ret = new Collection();
		foreach ($this->scene_children as $subscene) {
			$ret = $ret->merge($subscene->childrenFlatten());
		}
		$ret->add($this);
		return $ret;
	}
	
	public function childAdd($child, $child_id = '')
	{
		if (!$child_id) {
			$child_id = $child->scene_id . $child->scene_id_suffix;
		}
		$this->scene_children->put($child_id, $child);
	}
	
	public function childGet($child_id)
	{
		$ret = $this->scene_children->get($child_id);
		return ($ret) ?: new SceneBaseController();
	}
	
	
	public function sceneId()
	{
		$ret = $this->scene_id . $this->scene_id_suffix;
		if ($this->scene_parent) {
			$ret = $this->scene_parent->sceneId() . '-' . $ret;
		}
		return $ret;
	}
	
	public function sceneURL($parameters = [])
	{
		$url = $this->scene_url;
		if (!$url) {
			$url = action(static::methodPath('get'), $parameters);
		}
		return $url;
	}
	
	public function sceneVariables()
	{
		return ['scene' => $this] + get_object_vars($this);
	}
	
	
	public function sceneStateName($state, &$frontend_hidden = false) {
		$ret = $state;
		if (is_array($state)) {
			$ret = (!array_key_exists('name', $state)) ? '' : $state['name'];
			$frontend_hidden = (!array_key_exists('frontend_hidden', $state)) ? $frontend_hidden : $state['frontend_hidden'];
		}
		return $ret;
	}
	
	public function sceneStateArray()
	{
		$ret = [];
		foreach ($this->state as $state) {
			$state_name = $this->sceneStateName($state);
			if (!$state_name) {
				continue;
			}
			$ret[] = $state;
		}
		foreach ($this->state_permanent as $state_name => $state) {
			$state_name = $this->sceneStateName($state);
			if (!$state_name) {
				continue;
			}
			$do_add = true;
			foreach ($ret as $state_ret) {
				$state_name_ret = $this->sceneStateName($state_ret);
				if ($state_name === $state_name_ret) {
					$do_add = false;
					break;
				}
			}
			if ($do_add) {
				$ret[] = $state;
			}
		}
		return $ret;
	}
	
	public function sceneState($with_hidden = false)
	{
		$ret = [];
		$stts = $this->sceneStateArray();
		foreach($stts as $state) {
			$frontend_hidden = false;
			$state_name = $this->sceneStateName($state, $frontend_hidden);
			if ($with_hidden || !$frontend_hidden) {
				$ret[$state_name] = $this->$state_name;
			}
		}
		return $ret;
	}


	public function getTemplateContent()
	{
		$template = $this->template_content;
		if (!$template) {
			$scene_key = static::$scene_key;
			$path = str_replace('-', '.', $scene_key);
			$template = $path . '.' . $scene_key . '_content';
		}
		return View::exists($template) ? $template : '';
	}
	
	public function getTemplateCode()
	{
		$template = $this->template_code;
		if (!$template) {
			$scene_key = static::$scene_key;
			$path = str_replace('-', '.', $scene_key);
			$template = $path . '.' . $scene_key . '_code';
		}
		return View::exists($template) ? $template : '';
	}
	
	public function getLayout()
	{
		$layout = $this->layout;
		if ($layout && View::exists($layout)) {
			return $layout;
		}
		$layout = config('scene.default_layout');
		if ($layout && View::exists($layout)) {
			return $layout;
		}
		$layout = 'vendor.scene.layouts.scene-default.layout';
		if (View::exists($layout)) {
			return $layout;
		}
		$layout = 'scene-views::layouts.scene-default.layout';
		return $layout;
	}
	
	
	public function sceneResponse($response_data = [])
	{
		if ($this->scene_technology == 'ajax') {
			if (!is_array($response_data)) {
				$response_data = [];
			}
			
			if ($this->has_content_error) {
				$response_data += ['messages' => $this->messages->toArray('error')];
				return response()->json($response_data, 422);
			}
			
			$response_data += $this->sceneState();
			$response_data += ['messages' => $this->messages->toArray()];
			
			if ($this->has_content) {
				$content = view($this->getTemplateContent(), $this->sceneVariables());
				$response_data += ['content' => $content->render()];
			}
			
			return response()->json($response_data);
		} else {
			if ($this->has_content_error) {
				return $this->contentErrorRedirect();
			}
			return view($this->getLayout(), $this->sceneVariables());
		}
	}
	
	
	public function propertyToSession($name, $request = null, $default = null)
	{
		$val = $default;
		if (property_exists($this, $name) && !$default) {
			$val = $this->$name;
		}
		if ($request) {
			$val = $request->get($name, $val);
		}
		Session::flash('scene.'.$name, $val);
	}
	
	public function propertyFromSession($name, $default = null)
	{
		$val = $default;
		if (property_exists($this, $name) && !$default) {
			$val = $this->$name;
		}
		$val = Session::get('scene.'.$name, $val);
		
		if (property_exists($this, $name)) {
			$this->$name = $val;
		}
		return $val;
	}
	
	public function prepareGet($request)
	{
		$this->propertyFromSession('scene_id', static::$scene_key);
		$this->propertyFromSession('scene_technology', 'standard');
		$this->propertyFromSession('has_content_error', false);
		$this->messages->fromJSON(Session::get('scene.messages', '{}'));
		$prim = $this->sceneState(true);
		foreach(array_keys($prim) as $prim_key) {
			$this->propertyFromSession($prim_key);
		}
	}
	
	public function preparePost($request)
	{
		$this->scene_id = $request->get('scene_id', static::$scene_key);
		$this->scene_technology = ($request->ajax()) ? 'ajax' : 'standard';
		$stts = $this->sceneState(true);
		foreach(array_keys($stts) as $stt_key) {
			if (property_exists($this, $stt_key)) {
				$this->$stt_key = $request->get($stt_key, $this->$stt_key);
			}
		}
	}
	
	public function redirect($to = null, $status = 302, $headers = [], $secure = null)
	{
		$this->propertyToSession('scene_id');
		$this->propertyToSession('scene_technology');
		$this->propertyToSession('has_content_error');
		$this->propertyToSession('messages', null, $this->messages->toJSON());
		$stts = $this->sceneState(true);
		foreach(array_keys($stts) as $stt_key) {
			$this->propertyToSession($stt_key);
		}
		
		return redirect($to, $status, $headers, $secure); 
	}

	public function validate($request, array $rules, array $messages = [], array $customAttributes = [])
	{
		$request_data = (is_array($request)) ? $request : $request->all();
		$validator = $this->getValidationFactory()->make($request_data, $rules, $messages, $customAttributes);
		if ($validator->fails()) {
			$this->messages->addError($validator->errors()->messages());
			return false;
		}
		return true;
	}

	
	protected function handleBoolean($val)
	{
		$true_values = ['true', '1'];
		$false_values = ['null', 'false', '0'];
		
		if (in_array($val, $true_values)) return true;
		if (in_array($val, $false_values)) return false;
		
		return $val;
	}
	
	protected function handleRequestBoolean($name, $default = false, $request = null)
	{
		$ret = $request->input($name, $default);
		$ret = $this->handleBoolean($ret);
		return $ret;
	}
	
	
	public function setContentError($message = '')
	{
		$this->has_content_error = true;
		if ($message) {
			$this->messages->addError($message);
		}
	}
	
	public function contentErrorRedirect()
	{
		$this->has_content_error = false;
		return $this->redirect('');
	}

	public function prgRedirect($parameters = [])
	{
		return ($this->scene_technology == 'ajax')
			? ''
			: $this->redirect()->action($this->methodPath('get'), $parameters);
	}
	
	
	public static function methodPath($method_name)
	{
		return [static::class, $method_name];
	}

	public function getActionTechnology($action)
	{
		$ret = static::$default_actions_technology;
		if (array_key_exists($action, static::$actions_technologies)) {
			$ret = static::$actions_technologies[$action];
		}
		if ($this->is_nested) {
			$ret = 'ajax';
		}
		return $ret;
	}
}
