<?php
namespace DecideNow\Scene\Messages;

use Illuminate\Support\Collection;

class SceneMessages
{
	public $success;
	public $warning;
	public $error;
	
	public $tags;
	
	public function __construct()
	{
		$this->success = new Collection();
		$this->warning = new Collection();
		$this->error = new Collection();
		
		$this->tags = new \stdClass();
		$this->tags->success = []; 
		$this->tags->warning = [];
		$this->tags->error = [];		
	}
	
	public function clear($type = '')
	{
		if ($type == 'success' || $type == '') {
			$this->success->forget($this->success->keys()->all());
			$this->tags->success = [];
		}
		if ($type == 'warning' || $type == '') {
			$this->warning->forget($this->warning->keys()->all());
			$this->tags->warning = [];
		}
		if ($type == 'error' || $type == '') {
			$this->error->forget($this->error->keys()->all());
			$this->tags->error = [];
		}
	}
	
	public function add($message, $type, $tag = '')
	{
		if ($type == 'success') {
			$this->success->push('' . $message);
			if ($tag) {
				$last_key = $this->success->keys()->last();
				$this->tags->success[$last_key] = $tag;
			}
		}
		if ($type == 'warning') {
			$this->warning->push('' . $message);
			if ($tag) {
				$last_key = $this->warning->keys()->last();
				$this->tags->warning[$last_key] = $tag;
			}
		}
		if ($type == 'error') {
			$this->error->push('' . $message);
			if ($tag) {
				$last_key = $this->error->keys()->last();
				$this->tags->error[$last_key] = $tag;
			}
		}
	}
	
	public function addMessage($message, $type, $tag = '')
	{
		if (is_array($message)) {
			foreach ($message as $message_tag => $message_item) {
				if (is_array($message_item)) {
					foreach ($message_item as $message_text) {
						$this->add($message_text, $type, $message_tag);
					}
				} else {
					$this->add($message_item, $type, $tag);
				}
			}
		} else {
			$this->add($message, $type, $tag);
		}
	}
	
	public function addSuccess($message, $tag = '')
	{
		$this->addMessage($message, 'success', $tag);
	}
	
	public function addWarning($message, $tag = '')
	{
		$this->addMessage($message, 'warning', $tag);
	}
	
	public function addError($message, $tag = '')
	{
		$this->addMessage($message, 'error', $tag);
	}

	public function toArray($type = '') {
		$ret = [];
		$tags = [];
		if (($type == 'success' || $type == '') && $this->success->count()) {
			$ret['success'] = $this->success->toArray();
			if (count($this->tags->success)) {
				$tags['success'] = $this->tags->success;
			}
		}
		if (($type == 'warning' || $type == '') && $this->warning->count()) {
			$ret['warning'] = $this->warning->toArray();
			if (count($this->tags->warning)) {
				$tags['warning'] = $this->tags->warning;
			}
		}
		if (($type == 'error' || $type == '') && $this->error->count()) {
			$ret['error'] = $this->error->toArray();
			if (count($this->tags->error)) {
				$tags['error'] = $this->tags->error;
			}
		}
		if (count($tags)) {
			$ret['tags'] = $tags;
		}
		return $ret;
	}
	
	public function toJSON()
	{
		return json_encode($this->toArray());
	}
	
	public function fromJSON($messages)
	{
		$msg = json_decode($messages, true);
		if (array_key_exists('success', $msg)) {
			$this->clear('success');
			$this->addSuccess($msg['success']);
		}
		if (array_key_exists('warning', $msg)) {
			$this->clear('warning');
			$this->addWarning($msg['warning']);
		}
		if (array_key_exists('error', $msg)) {
			$this->clear('error');
			$this->addError($msg['error']);
		}
		if (array_key_exists('tags', $msg)) {
			if (array_key_exists('success', $msg['tags'])) {
				$this->tags->success = $msg['tags']['success'];
			}
			if (array_key_exists('warning', $msg['tags'])) {
				$this->tags->warning = $msg['tags']['warning'];
			}
			if (array_key_exists('error', $msg['tags'])) {
				$this->tags->error = $msg['tags']['error'];
			}
		}
	}
}

